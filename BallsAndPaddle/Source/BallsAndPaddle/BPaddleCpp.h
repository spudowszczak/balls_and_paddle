// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "BPaddleCpp.generated.h"

class USceneComponent;
class UStaticMesh;
class UBoxComponent;
class UCameraComponent;


UCLASS()
class BALLSANDPADDLE_API ABPaddleCpp : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABPaddleCpp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		USceneComponent* Scene;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UStaticMeshComponent* PaddleMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		UBoxComponent* Box;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		UCameraComponent* Camera;

	void MoveRight(float Value);

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	float PaddleSpeed=50.0f;

public:	


	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	
};
