// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BDeathWall.generated.h"


class UStaticMeshComponent;

UCLASS()
class BALLSANDPADDLE_API ABDeathWall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABDeathWall();

protected:
	UPROPERTY(EditDefaultsOnly,Category = "Component")
	UStaticMeshComponent* MeshComp;

public:	

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
	
};
