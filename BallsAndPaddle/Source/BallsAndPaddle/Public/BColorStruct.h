// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/UserDefinedStruct.h"
#include "BColorStruct.generated.h"

/**
 * 
 */
UCLASS()
class BALLSANDPADDLE_API UBColorStruct : public UUserDefinedStruct
{
	GENERATED_BODY()

	FColor Green = FColor::Green;
	FColor Yellow = FColor::Yellow;
	FColor Red = FColor::Red;

	
	
	
};
