// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BBallCpp.generated.h"


class UStaticMeshComponent;
class ABBrickCpp;

UCLASS()
class BALLSANDPADDLE_API ABBallCpp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABBallCpp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Component")
	UStaticMeshComponent* BallMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Default")
	int BPower;

public:	

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent,BlueprintCallable, Category = "Events")
	void SpeedUpcpp(int Accelerate);
	
	UFUNCTION(BlueprintCallable, Category = "Event")
	void AccelerateCpp();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Events")
		void Shrink();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Events")
		void Grow();

	/*UFUNCTION(BlueprintImplementableEvent,BlueprintCallable, Category = "Event")
		void BrickHitCpp(ABBrickCpp* Brick);*/


	
};
