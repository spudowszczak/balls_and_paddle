// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BPlayerController.generated.h"


/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FKillBall, ABBallCpp*, Ball, bool, bLost);	//killed actor,killer actor, 


class ABBallCpp;
UCLASS()
class BALLSANDPADDLE_API ABPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	ABPlayerController();
protected:
	

public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Actions")
	void FKillBall(ABBallCpp* Ball, bool bLost);
	// BlueprintAssignable
	
	
};
