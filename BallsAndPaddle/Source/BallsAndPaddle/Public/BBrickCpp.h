// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BBrickCpp.generated.h"


//USTRUCT(BlueprintType)
struct FColorB
{
	//GENERATED_BODY()

		FColor Green = FColor::Green;
	FColor Yellow = FColor::Yellow;
	FColor Red = FColor::Red;
	FColor Black = FColor::Black;
};

UCLASS()
class BALLSANDPADDLE_API ABBrickCpp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABBrickCpp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
		UStaticMeshComponent* BrickMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Component")
	UMaterialInstanceDynamic* MatInst;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
	int BBrickLife = 1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Component")
	int BBrickDeathScore = 10;

	/*UPROPERTY(EditDefaultsOnly, Category = "Component")
	UMaterialInterface* MatInt;


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
	UMaterial* AssetMaterial;
	*/


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION( BlueprintCallable, Category = "Events")
		bool GetHitCpp();

	UFUNCTION(BlueprintCallable, Category = "Events")
		void UpdateColorCpp();
	
	//Sets bonus points for breaking block
	UFUNCTION(BlueprintCallable, Category = "Events")
		void InitBonusCpp();
	//TODO check is point counts in proper way
};
