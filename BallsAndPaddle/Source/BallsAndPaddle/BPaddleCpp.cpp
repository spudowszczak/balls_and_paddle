// Fill out your copyright notice in the Description page of Project Settings.

#include "BPaddleCpp.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SceneComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values
ABPaddleCpp::ABPaddleCpp()
{
	Scene  = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = Scene;

	PaddleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PaddleMesh"));
	PaddleMesh->AttachTo(Scene);
	PaddleMesh->SetRelativeScale3D(FVector(0.3f, 1.95f, 0.4f));

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	Box->SetupAttachment(PaddleMesh);
	Box->SetUseCCD(true);
	Box->SetCollisionResponseToAllChannels(ECR_Block);
	Box->SetBoxExtent(FVector(128.0f, 128.0f, 128.0f));

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(Scene);
	Camera->SetRelativeLocation(FVector(10.0f, 0.0f, 1750.0f));
	Camera->SetRelativeRotation(FRotator(-70.0f, 0.0f, 0.0f));

}

// Called when the game starts or when spawned
void ABPaddleCpp::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called to bind functionality to input
void ABPaddleCpp::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Right", this, &ABPaddleCpp::MoveRight);

}

void ABPaddleCpp::MoveRight(float Value)
{
	float tempLoc = Value * PaddleSpeed;
	FVector tempVec = FVector(0.0f, tempLoc, 0.0f);
	PaddleMesh->AddLocalOffset(tempVec, true);
}

