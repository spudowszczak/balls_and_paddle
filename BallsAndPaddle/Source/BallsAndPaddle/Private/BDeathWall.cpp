// Fill out your copyright notice in the Description page of Project Settings.

#include "BDeathWall.h"
#include "Components/StaticMeshComponent.h"
#include "BBallCpp.h"
#include "BPlayerController.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
ABDeathWall::ABDeathWall()
{
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;
	MeshComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComp->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshComp->bGenerateOverlapEvents = true;

	//Finds and sets mesh
	static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));
	UStaticMesh* Asset = MeshAsset.Object;
	MeshComp->SetStaticMesh(Asset);

	//Finds and sets material to mesh
	static ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("Material'/Game/Materials/M_DeathWall.M_DeathWall'"));
	if (MaterialAsset.Succeeded())
	{
		UE_LOG(LogTemp, Warning, TEXT("material loaded"));
		UMaterial* AssetMaterial = MaterialAsset.Object;
		UMaterialInterface* MatInt = Cast<UMaterialInterface>(MaterialAsset.Object);
		MeshComp->SetMaterial(0, MatInt);
	}

}

void ABDeathWall::NotifyActorBeginOverlap(AActor * OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	ABBallCpp* Ball = Cast<ABBallCpp>(OtherActor);
	if (Ball) 
	{
		ABPlayerController* BPC = Cast<ABPlayerController>(Ball->GetOwner());
		if (BPC)
		{
			BPC->FKillBall(Ball, true);
		}
	}

}
