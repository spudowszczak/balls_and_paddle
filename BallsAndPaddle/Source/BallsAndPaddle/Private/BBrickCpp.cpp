// Fill out your copyright notice in the Description page of Project Settings.

#include "BBrickCpp.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Collision.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Engine/World.h"

// Sets default values
ABBrickCpp::ABBrickCpp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BrickMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BrickMesh"));
	RootComponent = BrickMesh;

	static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));
	UStaticMesh* Asset = MeshAsset.Object;
	BrickMesh->SetStaticMesh(Asset);
	BrickMesh->SetSimulatePhysics(false);
	//BallMesh->SetNotifyRigidBodyCollision(true);

	//Finds and sets material to mesh
	static ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("Material'/Game/Materials/M_Brick.M_Brick'"));
	if (MaterialAsset.Succeeded())
	{
		UE_LOG(LogTemp, Warning, TEXT("material loaded"));
		UMaterial* AssetMaterial = MaterialAsset.Object;
		UMaterialInterface* MatInt = Cast<UMaterialInterface>(MaterialAsset.Object);
		BrickMesh->SetMaterial(0, MatInt);
	}

}



// Called when the game starts or when spawned
void ABBrickCpp::BeginPlay()
{
	Super::BeginPlay();
	if (MatInst == nullptr)
	{
		MatInst = BrickMesh->CreateAndSetMaterialInstanceDynamicFromMaterial(0, BrickMesh->GetMaterial(0));
	}
}

// Called every frame
void ABBrickCpp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ABBrickCpp::GetHitCpp()
{

	
	MatInst->SetScalarParameterValue(TEXT("LastTimeTakenDamage"), GetWorld()->GetTimeSeconds());
	BBrickLife--;
	UpdateColorCpp();
	if (BBrickLife <= 0) 
	{
		BrickMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		SetLifeSpan(0.2f);
		return true;
	}

	return false;
}

void ABBrickCpp::UpdateColorCpp()
{
	FColorB myFColorB;

	switch (BBrickLife) {
	case 1:
		MatInst->SetVectorParameterValue(TEXT("Color"), myFColorB.Green);
		break;
	case 2:
		MatInst->SetVectorParameterValue(TEXT("Color"), myFColorB.Yellow);
		break;
	case 3:
		MatInst->SetVectorParameterValue(TEXT("Color"), myFColorB.Red);
		break;
	default:
		MatInst->SetVectorParameterValue(TEXT("Color"), myFColorB.Black);
	}
}

void ABBrickCpp::InitBonusCpp()
{
	switch (BBrickLife) 
	{
	case  1:
		BBrickDeathScore = 10;
		break;
	case  2:
		BBrickDeathScore = 20;
		break;
	case  3:
		BBrickDeathScore = 50;
		break;
	default:
		BBrickDeathScore = 1;
		break;
	}
}



