// Fill out your copyright notice in the Description page of Project Settings.

#include "BBallCpp.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Collision.h"
#include "BBrickCpp.h"




// Sets default values
ABBallCpp::ABBallCpp()
{
	PrimaryActorTick.bCanEverTick = true;

	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BallMesh"));
	RootComponent = BallMesh;

	//Finds and sets mesh
	static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	UStaticMesh* Asset = MeshAsset.Object;
	BallMesh->SetStaticMesh(Asset);
	BallMesh->SetLinearDamping(0.0f);
	BallMesh->SetEnableGravity(false);
	BallMesh->SetSimulatePhysics(false);
	BallMesh->SetConstraintMode(EDOFMode::XYPlane);
	BallMesh->SetNotifyRigidBodyCollision(true);


	BPower = 400;

}

void ABBallCpp::BeginPlay()
{
	Super::BeginPlay();
	
}


void ABBallCpp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AccelerateCpp();
}

void ABBallCpp::AccelerateCpp()
{
	//Runs only when Ball is simulating physics and gets slow down under certain speed
	if (!BallMesh->IsSimulatingPhysics()) 
	{
		return;
	}

	if (BallMesh->GetComponentVelocity().Size2D() < BPower*3.0f)
	{
		BallMesh->AddForce(BallMesh->GetComponentVelocity()*(BPower / 50.0f), NAME_None, true);
	}

}

/*void ABBallCpp::BrickHitCpp(ABBrickCpp* Brick)
{
	//Brick->GetHitCpp();
	
}*/

